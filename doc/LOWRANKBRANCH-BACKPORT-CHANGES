CHANGES REQUIRING CODE ADAPTATION
---------------------------------
---------------------------------

* file dune/solvers/norms/diagnorm.hh : class DiagNorm
    ° introduced template parameters in order to use it with arbitrary matrix blocks
        \tparam Diagonal some container type having the random access operator[]; used to store the diagonal entries of the matrix
        \tparam VectorType the vector type the norm may be applied to
    ° old implementation remains as template specialization
---------------------------------------------------------------------------------------
!   ° REQUIRED CODE CHANGES: where "DiagNorm" was used, now "DiagNorm<>" is required  !
---------------------------------------------------------------------------------------

* file dune/solvers/norms/fullnorm.hh : class FullNorm
    ° introduced template parameters in order to use it with arbitrary matrix blocks
        \tparam LowRankFactor the type of the factor used to represent the low rank operator
        \tparam VectorType the vector type the norm may be applied to
    ° old implementation remains as template specialization
---------------------------------------------------------------------------------------
!   ° REQUIRED CODE CHANGES: where "FullNorm" was used, now "FullNorm<>" is required  !
---------------------------------------------------------------------------------------


CHANGES POSSIBLY AFFECTING PERFOMANCE (INCLUDING BUGFIXES)
----------------------------------------------------------
----------------------------------------------------------

* file dune/solvers/common/staticmatrixtools.hh
    ° bugfix for addToDiagonal concerning template specialization

* file dune/solvers/norms/energynorm.hh
    ° catch slightly negative values of normSquared and return zero instead
    ° "negativity tolerance" may be set in constructor; default is 1e-10

EXTENSION OF FUNCTIONALITY
--------------------------
--------------------------

* file dune/solvers/transferoperators/compressedmultigridtransfer.hh : class CompressedMultigridTransfer
    ° introduced template specialization for matrices of SumOperator<SparseMatrixType, LowRankMatrixType>-type

* file dune/solvers/common/staticmatrixtools.hh
    ° added some type promotions
    (° bugfix for addToDiagonal concerning template specialization)

* file dune/solvers/common/genericvectortools.hh : 
    ° added template specialization of method static void resize for VectorTypeB=SumOperator<...>

* file dune/solvers/iterationsteps/truncatedblockgsstep.hh :
    ° introduced template specialization for matrices of SumOperator<SparseMatrixType, LowRankMatrixType>-type

* file dune/solvers/operators/nulloperator.hh :
    ° introduced method umtv
    ° introduced method usmtv
    ° export field_type and size_type
    ° introduced methods M(), N()
    ° introduced operator*=
    ° introduced operator[]
    ° introduced operator[] in RowDummy

* file dune/solvers/operators/lowrankoperator.hh :
    ° lowRankFactor_ now stored as a pointer
    ° introduced default constructor
    ° introduced method mmv
    (° renamed method getLowRankFactor->lowrankFactor)
    ° added nonconst getter lowrankFactor

* file dune/solvers/operators/sumoperator.hh :
    ° summands now stored as pointers
    ° introduced default constructor
    ° introduced destructor
    ° introduced method mmv
    ° introduced methods M(), N()
    (° renamed method getSOMETHINGMatrix->SOMETHINGMatrix)
    ° added nonconst getters

FURTHER CHANGES
-------------------------------------
-------------------------------------

* copied file dune/solvers/common/arithmetic.hh from dune-fufem

* file dune/solvers/transferoperators/truncatedcompressedmgtransfer.cc
    ° harmonized template parameter names with base class

* file dune/solvers/test/sumoperatortest.cc
    ° extended test, now also testing
        default construction
        getter methods
        consistency of N()

* file dune/solvers/test/lowrankoperatortest.cc
    ° extended test, now also testing
        method mmv
        for fixed block sizes 1...4

* use size_t instead of int in order to avoid signed/unsigned mismatch warnings
    ° file dune/solvers/iterationsteps/truncatedblockgsstep.hh
    ° file dune/solvers/iterationsteps/multigridstep.hh
    ° file dune/solvers/norms/fullnorm.hh
    ° file dune/solvers/norms/diagnorm.hh
    ° file dune/solvers/common/staticmatrixtools.hh

* file dune/solvers/solvers/loopsolver.cc: initialize value in order to avoid warning

* fixed some typos/white space issues
