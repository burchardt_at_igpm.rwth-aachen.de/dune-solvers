# searches for ipopt headers and libs

AC_DEFUN([DUNE_IPOPT],[
  AC_REQUIRE([AC_PROG_CXX])
  AC_CHECK_HEADERS(cstddef stddef.h)

  AC_ARG_WITH(ipopt,
    AC_HELP_STRING([--with-ipopt=PATH],[directory with IPOpt inside]))

# store values
ac_save_LDFLAGS="$LDFLAGS"
ac_save_CPPFLAGS="$CPPFLAGS"
ac_save_LIBS="$LIBS"
ac_save_PKG_CONFIG_PATH="$PKG_CONFIG_PATH"

## do nothing if --without-ipopt is used
AS_IF([test x$with_ipopt != xno], [
    # is --with-ipopt=bla used?
    AS_IF([test "x$with_ipopt" != xyes], [
        AS_IF([! test -d $with_ipopt], [
            AC_MSG_WARN([IPOpt directory $with_ipopt does not exist])
        ], [
            # expand tilde / other stuff
            IPOPTROOT=`cd $with_ipopt && pwd`
        ])
    ])
    AS_IF([test "x$IPOPTROOT" = x], [
	# use some default value...
	IPOPTROOT="/usr/local/ipopt"
    ])

    IPOPT_LIB_PATH="$IPOPTROOT/lib"

    # Check for ipopt and set IPOPT_*
    export PKG_CONFIG_PATH="$IPOPT_LIB_PATH/pkgconfig:$PKG_CONFIG_PATH"
    PKG_CHECK_MODULES([IPOPT], [ipopt], [
	HAVE_IPOPT="1"
    ], [ dnl default rule would fail
	AC_MSG_WARN([IPOpt not found in $IPOPTROOT])
    ])

    dnl For some strange reason, PKG_CHECK_MODULES puts the stuff we would expect
    dnl in IPOPT_CPPFLAGS (namely, -I<path>)in IPOPT_CFLAGS with ipopt 3.10.1.
    dnl We therefore copy it by hand
    IPOPT_CPPFLAGS="$IPOPT_CFLAGS -DENABLE_IPOPT"
    
    LDFLAGS="$LDFLAGS $IPOPT_LDFLAGS"
    CPPFLAGS="$CPPFLAGS $IPOPT_CPPFLAGS"
    LIBS="$LIBS $IPOPT_LIBS"

])

with_ipopt="no"
# survived all tests?
AS_IF([test x$HAVE_IPOPT = x1], [
  AC_SUBST(IPOPT_LIBS, $IPOPT_LIBS)
  AC_SUBST(IPOPT_LDFLAGS, $IPOPT_LDFLAGS)
  AC_SUBST(IPOPT_CPPFLAGS, $IPOPT_CPPFLAGS)
  AC_DEFINE(HAVE_IPOPT, [ENABLE_IPOPT], [Define to ENABLE_IPOPT if IPOpt library is found])

  # add to global list
  DUNE_PKG_LDFLAGS="$DUNE_PKG_LDFLAGS $IPOPT_LDFLAGS"
  DUNE_PKG_LIBS="$DUNE_PKG_LIBS $IPOPT_LIBS"
  DUNE_PKG_CPPFLAGS="$DUNE_PKG_CPPFLAGS $IPOPT_CPPFLAGS"

  # set variable for summary
  with_ipopt="yes"
], [
  unset IPOPT_LIBS
  AC_SUBST(IPOPT_LIBS, "")
  unset IPOPT_LDFLAGS
  AC_SUBST(IPOPT_LDFLAGS, "")
  unset IPOPT_CPPFLAGS
  AC_SUBST(IPOPT_CPPFLAGS, "")
])
  
# also tell automake
AM_CONDITIONAL(IPOPT, test x$HAVE_IPOPT = x1)

# reset old values
LIBS="$ac_save_LIBS"
CPPFLAGS="$ac_save_CPPFLAGS"
LDFLAGS="$ac_save_LDFLAGS"
PKG_CONFIG_PATH="$ac_save_PKG_CONFIG_PATH"

DUNE_ADD_SUMMARY_ENTRY([IPOpt],[$with_ipopt])

])
