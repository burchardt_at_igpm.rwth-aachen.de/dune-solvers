// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DIAGNORM_HH
#define DIAGNORM_HH

#include <cmath>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include "norm.hh"

/**  \brief Vector norm induced by (block) diagonal linear operator
  *
  *  \tparam Diagonal some container type having the random access operator[]; used to store the diagonal entries of the matrix
  *  \tparam VectorType the vector type the norm may be applied to
  */
template <class Diagonal=Dune::BlockVector<Dune::FieldVector <double,1> >, class V=Dune::BlockVector<Dune::FieldVector <double,1> > >
class DiagNorm:
    public Norm<V>
{
    public:
        typedef V VectorType;
        typedef typename VectorType::field_type field_type;
    private:
        typedef typename VectorType::size_type SizeType;

    public:
        DiagNorm(const field_type alpha, const Diagonal &d) :
            d(d),
            alpha(alpha)
        {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &f) const
        {
            return std::sqrt(normSquared(f));
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const
        {
            field_type r = 0.0;

            typename VectorType::value_type Dv(0.0);
            for(SizeType row = 0; row < f.size(); ++row)
            {
                d[row].mv(f[row],Dv);
                r += Dv*f[row];
            }

            return std::abs(alpha * r);
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &f1, const VectorType &f2) const
        {
            field_type r = 0.0;

            typename VectorType::value_type Dv(0.0);
            for(SizeType row = 0; row < f1.size(); ++row)
            {
                d[row].mv(f1[row]-f2[row],Dv);
                r += Dv*(f1[row]-f2[row]);
            }

            return std::sqrt(std::abs(alpha * r));
        }

    private:
        const Diagonal& d;

        const field_type alpha;

};

template<>
class DiagNorm<Dune::BlockVector<Dune::FieldVector <double,1> >, Dune::BlockVector<Dune::FieldVector <double,1> > >:
    public Norm<Dune::BlockVector<Dune::FieldVector <double,1> > >
{
    public:
        typedef double field_type;
        typedef Dune::BlockVector<Dune::FieldVector <field_type,1> > VectorType;
    private:
        typedef VectorType::size_type SizeType;

    public:
        DiagNorm(const field_type alpha, const VectorType &d) :
            d(d),
            alpha(alpha)
        {}

        //! Compute the norm of the given vector
        field_type operator()(const VectorType &f) const
        {
            return std::sqrt(normSquared(f));
        }

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const
        {
            field_type r = 0.0;

            for(SizeType row = 0; row < f.size(); ++row)
                r += d[row] * f[row] * f[row];

            return std::abs(alpha * r);
        }

        //! Compute the norm of the difference of two vectors
        field_type diff(const VectorType &f1, const VectorType &f2) const
        {
            field_type r = 0.0;

            for (SizeType row = 0; row < f1.size(); ++row)
                r += (field_type)d[row] * (f1[row]-f2[row]) * (f1[row] - f2[row]);

            return std::sqrt(std::abs(alpha * r));
        }

    private:
        const VectorType &d;

        const field_type alpha;

};

#endif 
