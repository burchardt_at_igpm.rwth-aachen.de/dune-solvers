// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef NORM_HH
#define NORM_HH

//! Abstract base for classes computing norms of discrete functions
template <class V>
class Norm {

    public:
        typedef V VectorType;

        typedef typename VectorType::field_type field_type;

        /** \brief Destructor, doing nothing */
        virtual ~Norm() {};

        //! Compute the norm of the given vector
        virtual field_type operator()(const VectorType& f) const = 0;

        //! Compute the square of the norm of the given vector
        virtual field_type normSquared(const VectorType& f) const
        {
            field_type r = this->operator()(f);
            return r*r;
        }

        //! Compute the norm of the difference of two vectors
        virtual field_type diff(const VectorType& f1, const VectorType& f2) const = 0;

};

#endif
