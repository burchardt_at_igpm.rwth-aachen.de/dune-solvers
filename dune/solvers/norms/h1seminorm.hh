// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_H1_SEMINORM_HH
#define DUNE_H1_SEMINORM_HH

#include <cmath>

#include <dune/common/fmatrix.hh>
#include <dune/istl/bcrsmatrix.hh>

#include "norm.hh"

//! Specialisation of the EnergyNorm class to identity blocks
template<class VectorType>
class H1SemiNorm : public Norm<VectorType>
{
public:
    H1SemiNorm() : matrix_(NULL) {}

    H1SemiNorm(const Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >& matrix)
        : matrix_(&matrix)
    {
      assert(matrix.N() == matrix.M());
    }

    //! Compute the norm of the difference of two vectors
    double diff(const VectorType& u1, const VectorType& u2) const
	{
          assert(u1.size()==u2.size());
          assert(u1.size()==matrix_->N());

		double sum = 0;

		for (size_t i=0; i<matrix_->N(); i++)
		{
			typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::const_iterator cIt    = (*matrix_)[i].begin();
			typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::const_iterator cEndIt = (*matrix_)[i].end();

                        typename VectorType::block_type differ_i = u1[i] - u2[i];
			for (; cIt!=cEndIt; ++cIt)
				sum += differ_i * (u1[cIt.index()]-u2[cIt.index()]) * (*cIt);
        }

        return std::sqrt(sum);
    }

    //! Compute the norm of the given vector
    double operator()(const VectorType& u) const
    {
        if (matrix_ == NULL)
            DUNE_THROW(Dune::Exception, "You have not supplied neither a matrix nor an IterationStep to the EnergyNorm routine!");

        assert(u.size()==matrix_->N());

        // we compute sqrt{uAu^t}.  We have implemented this by hand because the matrix is
        // always scalar but the vectors may not be
        double sum = 0;

        for (size_t i=0; i<matrix_->N(); i++) {

            typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::const_iterator cIt    = (*matrix_)[i].begin();
            typename Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >::row_type::const_iterator cEndIt = (*matrix_)[i].end();

            for (; cIt!=cEndIt; ++cIt)
                sum += u[i]*u[cIt.index()] * (*cIt);

        }

        return std::sqrt(sum);
    }

    const Dune::BCRSMatrix<Dune::FieldMatrix<double,1,1> >* matrix_;

};

#endif
