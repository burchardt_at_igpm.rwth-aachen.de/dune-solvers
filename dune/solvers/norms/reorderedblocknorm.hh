// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef REORDERED_BLOCK_NORM_HH
#define REORDERED_BLOCK_NORM_HH

#include <memory>
#include <vector>
#include <cmath>

#include <dune/istl/bvector.hh>

#include "../common/genericvectortools.hh"
#include "norm.hh"

//! A norm for blocks of interlaced vectors
template <class VectorType, class ReorderedLocalVector>
class ReorderedBlockNorm: public Norm<VectorType>
{
    public:
        typedef Dune::BlockVector<ReorderedLocalVector> ReorderedVector;

        typedef std::vector< std::shared_ptr<const Norm<ReorderedLocalVector> > > NormPointerVector;

        ReorderedBlockNorm(const NormPointerVector& norms) :
            norms_(norms)
        {}

        //! Compute the norm of the given vector
        double operator()(const VectorType &v) const
        {
            return std::sqrt(normSquared(v));
        }

        //! Compute the square of the norm of the given vector
        double normSquared(const VectorType& v) const
        {
            double r = 0.0;

            ReorderedVector v_r;
            GenericVector::deinterlace(v,v_r);

            for (int i=0; i<norms_.size(); ++i)
            {
                double ri = (*norms_[i])(v_r[i]);
                r += ri*ri;
            }
            return r;
        }

        //! Compute the norm of the difference of two vectors
        double diff(const VectorType &v1, const VectorType &v2) const
        {
            double r = 0.0;

            ReorderedVector v1_r, v2_r;
            GenericVector::deinterlace(v1,v1_r);
            GenericVector::deinterlace(v2,v2_r);

            for (size_t i=0; i<norms_.size(); ++i)
            {
//                std::cout << "norms.size()= " << norms_.size() << ", v1_r.size= " << v1_r.size() << ", v2_r.size= " << v2_r.size() << std::endl;
//                std::cout << "v1_r[" << i << "].size= " << v1_r[i].size() << ", v2_r[i].size= " << v2_r[i].size() << std::endl;
//                std::cout <<  v1_r[i] << std::endl << v2_r[i] << std::endl;
                double ri = (*norms_[i]).diff(v1_r[i], v2_r[i]);
                r += ri*ri;
            }
            return std::sqrt(r);
        }

    private:
        const NormPointerVector& norms_;
};

#endif
