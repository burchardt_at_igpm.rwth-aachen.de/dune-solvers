// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_PNORM_HH
#define DUNE_SOLVERS_PNORM_HH

#include <cmath>

#include <dune/common/fvector.hh>
#include <dune/istl/bvector.hh>

#include "norm.hh"

typedef Dune::BlockVector<Dune::FieldVector <double,1> > Vector;

class PNorm: public Norm<Vector>
{
	public:
		PNorm(int p=2, double alpha=1.0):
			p(p),
			alpha(alpha)
		{}
		
		double operator()(const Vector &v) const
		{
			double r = 0.0;
			
			if (p<1)
			{
				for(int row = 0; row < v.size(); ++row)
				{
					double z = std::abs(v[row]);
					if (r<z)
						r = z;
				}
			}
			else if (p==1)
			{
				for(int row = 0; row < v.size(); ++row)
					r += std::abs(v[row]);
			}
			else if (p==2)
			{
				for(int row = 0; row < v.size(); ++row)
					r += v[row] * v[row];
				r = std::sqrt(r);
			}
			else
			{
				for(int row = 0; row < v.size(); ++row)
					r += pow(std::abs(v[row]), p);
				r = pow(r, 1.0/p);
			}
			
			return alpha*r;
		}

		double diff(const Vector &v1, const Vector &v2) const
		{
			double r = 0.0;
			
			if (p<1)
			{
				for(int row = 0; row < v1.size(); ++row)
				{
					double z = std::abs(v1[row]-v2[row]);
					if (z>r)
						r = z;
				}
			}
			else if (p==1)
			{
				for(int row = 0; row < v1.size(); ++row)
					r += std::abs(v1[row]-v2[row]);
			}
			else if (p==2)
			{
				for(int row = 0; row < v1.size(); ++row)
					r += (v1[row]-v2[row]) * (v1[row]-v2[row]);
				r = std::sqrt(r);
			}
			else
			{
				for(int row = 0; row < v1.size(); ++row)
					r += pow(std::abs(v1[row]-v2[row]), p);
				r = pow(r, 1.0/p);
			}
			
			return alpha*r;
		}

	private:
		double alpha;
		double p;
		
};

#endif 
