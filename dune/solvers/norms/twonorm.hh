// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef TWONORM_HH
#define TWONORM_HH

#include <cmath>
#include <cstring> // For size_t

#include <dune/common/fvector.hh>

#include "norm.hh"

//! Wrapper around the two_norm() method of a vector class
template <class VectorType>
class TwoNorm : public Norm<VectorType>
{

    public:

        /** \brief Destructor, doing nothing */
        virtual ~TwoNorm() {};

        //! Compute the norm of the given vector
        virtual double operator()(const VectorType& f) const
        {
            return f.two_norm();
        }

        //! Compute the square of the norm of the given vector
        virtual double normSquared(const VectorType& f) const
        {
            return f.two_norm2();
        }

        //! Compute the norm of the difference of two vectors
        virtual double diff(const VectorType& f1, const VectorType& f2) const
        {
            assert(f1.size() == f2.size());
            double r = 0.0;

            for (size_t i=0; i<f1.size(); ++i)
            {
                typename VectorType::block_type block = f1[i];
                block -= f2[i];

                r += block.two_norm2();
            }

            return std::sqrt(r);
        }

};

template <typename T, int dimension>
class TwoNorm<Dune::FieldVector<T, dimension> >
  : public Norm<Dune::FieldVector<T, dimension> >
{
    typedef Dune::FieldVector<T, dimension> VectorType;

    public:

        /** \brief Destructor, doing nothing */
        virtual ~TwoNorm() {};

        //! Compute the norm of the given vector
        virtual double operator()(const VectorType& f) const
        {
            return f.two_norm();
        }

        //! Compute the square of the norm of the given vector
        virtual double normSquared(const VectorType& f) const
        {
            return f.two_norm2();
        }

        //! Compute the norm of the difference of two vectors
        virtual double diff(const VectorType& f1, const VectorType& f2) const
        {
          VectorType tmp = f1;
          tmp -= f2;
          return tmp.two_norm();
        }
};

#endif
