// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIONSTEPS_LINEARITERATIONSTEP_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_LINEARITERATIONSTEP_HH

#include <memory>
#include <vector>

#include <dune/common/bitsetvector.hh>
#include <dune/common/shared_ptr.hh>

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/common/defaultbitvector.hh>

#include "iterationstep.hh"

namespace Dune {

namespace Solvers {

/** \brief Base class for iteration steps being called by an iterative solver for linear problems
 *
 * This class is called Linear, because all iteration step implementations that derive from it
 * are supposed to solve linear problems.  In particular, this means that the LinearIterationStep
 * class knows about matrix data types, and it has the setProblem and setMatrix methods that
 * allow to specify the matrix of the linear system to solve.
 *
 * The 'Linear' in the name is not about the type of algorithm.  For example, one step of a
 * CG solver would qualify as a LinearIterationStep: CG is not a linear algorithm, but it is
 * a solver for solving linear problems.
 */
template<class MatrixType, class VectorType, class BitVectorType = Dune::Solvers::DefaultBitVector_t<VectorType> >
class LinearIterationStep : public IterationStep<VectorType, BitVectorType>,
                            public Dune::Solvers::Preconditioner<MatrixType,
                                                                 VectorType,
                                                                 BitVectorType>
{
public:

    //! Default constructor
    LinearIterationStep() {
        rhs_ = nullptr;
    }

    /** \brief Destructor */
    virtual ~LinearIterationStep() {}

    //! Constructor being given linear operator, solution and right hand side
    LinearIterationStep(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
      setProblem(mat, x, rhs);
    }

    //! Set linear operator, solution and right hand side
    virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs) {
        setProblem(x);
        rhs_     = &rhs;
        setMatrix(mat);
    }

    //! Set linear operator
    virtual void setMatrix(const MatrixType& mat) {
        mat_     = Dune::stackobject_to_shared_ptr(mat);
    }

    //! Do the actual iteration
    virtual void iterate() = 0;

    //! Return linear operator
    virtual const MatrixType* getMatrix() {return mat_.get();}

    /** \brief Checks whether all relevant member variables are set
     * \exception SolverError if the iteration step is not set up properly
     */
    virtual void check() const {
#if 0
        if (!x_)
            DUNE_THROW(SolverError, "Iteration step has no solution vector");
        if (!rhs_)
            DUNE_THROW(SolverError, "Iteration step has no right hand side");
        if (!mat_)
            DUNE_THROW(SolverError, "Iteration step has no matrix");
#endif
    }

    virtual void apply(VectorType& x, const VectorType& r)
    {
        x = 0;
        this->x_ = &x;
        rhs_     = &r;
        this->preprocess();
        iterate();
        x = this->getSol();
    }

    //! The container for the right hand side
    const VectorType* rhs_;

    //! The linear operator
    std::shared_ptr<const MatrixType> mat_;

private:
    using Base = IterationStep<VectorType, BitVectorType>;
    using Base::setProblem;
};

}   // namespace Solvers
}   // namespace Dune

// For backward compatibility: will be removed eventually
using Dune::Solvers::LinearIterationStep;

#endif
