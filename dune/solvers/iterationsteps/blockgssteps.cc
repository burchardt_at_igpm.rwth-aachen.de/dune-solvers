#include <algorithm>

#include "blockgssteps.hh"

namespace Dune {
namespace Solvers {
  namespace BlockGS {
    std::istream& operator>>(std::istream& lhs, Direction& t) {
      std::string s;
      lhs >> s;
      std::transform(s.begin(), s.end(), s.begin(), ::tolower);

      if (s == "forward")
        t = Direction::FORWARD;
      else if (s == "backward")
        t = Direction::BACKWARD;
      else if (s == "symmetric")
        t = Direction::SYMMETRIC;
      else
        lhs.setstate(std::ios_base::failbit);
      return lhs;
    }
  }

  std::istream& operator>>(std::istream& lhs, BlockGSType& t) {
    std::string s;
    lhs >> s;
    std::transform(s.begin(), s.end(), s.begin(), ::tolower);

    if (s == "direct")
      t = BlockGSType::Direct;
    else if (s == "ldlt")
      t = BlockGSType::LDLt;
    else if (s == "cg")
      t = BlockGSType::CG;
    else if (s == "gs")
      t = BlockGSType::GS;
    else
      lhs.setstate(std::ios_base::failbit);
    return lhs;
  }
}
}
