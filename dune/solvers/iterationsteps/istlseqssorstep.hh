// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef ISTL_SEQSSOR_STEP_HH
#define ISTL_SEQSSOR_STEP_HH

/** \file
    \brief A wrapper class for the ISTL SeqSSOR implementation
 */

#include <memory>

#include <dune/solvers/iterationsteps/lineariterationstep.hh>

#include <dune/istl/preconditioners.hh>

/** \brief A wrapper class for the ISTL SeqSSOR implementation
 */
template <class MatrixType, class VectorType>
class ISTLSeqSSORStep
    // FIXME: ignoreNodes are not handled
    : public LinearIterationStep<MatrixType, VectorType>
{
    typedef Dune::SeqSSOR<MatrixType,VectorType,VectorType> SeqSSOR;

public:
    ISTLSeqSSORStep (int iterations, double relaxationFactor)
        : iterations_(iterations), relaxationFactor_(relaxationFactor)
    {}

    /** \brief Sets up an algebraic hierarchy
     */
    virtual void preprocess() override {
        seqSSOR_ = std::make_unique<SeqSSOR>(*this->mat_, iterations_,
                                             relaxationFactor_);

        // Note: as of now, pre() is a dummy
        mutable_rhs = *this->rhs_;
        seqSSOR_->pre(*this->x_, mutable_rhs);
    }

    /** \brief Perform one iteration */
    virtual void iterate() override {
        mutable_rhs = *this->rhs_;
        seqSSOR_->apply(*this->x_, mutable_rhs);
    }

private:

    /** \brief The dune-istl sequential SSOR preconditioner */
    std::unique_ptr<SeqSSOR> seqSSOR_;

    VectorType mutable_rhs;

    int const iterations_;
    double const relaxationFactor_;
};
#endif
