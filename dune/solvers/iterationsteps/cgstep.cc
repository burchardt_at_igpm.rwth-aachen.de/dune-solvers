// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <dune/solvers/common/canignore.hh>

template <class MatrixType, class VectorType>
void CGStep<MatrixType, VectorType>::check() const
{
}

template <class MatrixType, class VectorType>
void CGStep<MatrixType, VectorType>::preprocess()
{
    // Compute the residual (r starts out as the rhs)
    matrix_->mmv(*x_,r_);
    if (this->hasIgnore())
        for (size_t i=0; i < r_.size(); ++i)
            if (this->ignore()[i].any())
                r_[i] = 0;

    if (preconditioner_) {
        using CanIgnore_t = CanIgnore<DefaultBitVector_t<VectorType>>;
        CanIgnore_t *asCanIgnore = dynamic_cast<CanIgnore_t*>(preconditioner_);
        if (asCanIgnore != nullptr and this->hasIgnore())
            asCanIgnore->setIgnore(this->ignore());

        preconditioner_->setMatrix(*matrix_);
        preconditioner_->apply(p_, r_);
    } else
        p_ = r_;

    r_squared_old_ = p_*r_;
}

template <class MatrixType, class VectorType>
void CGStep<MatrixType, VectorType>::iterate()
{
    // Avoid divide-by-zero. If r_squared was zero, we're done anyway.
    if (r_squared_old_ <= 0)
        return;

    VectorType q(*x_);

    matrix_->mv(p_, q);                             // q_0     = Ap_0
    const double alpha = r_squared_old_ / (p_ * q); // alpha_0 = r_0*r_0/p_0*Ap_0
    x_->axpy(alpha, p_);                            // x_1     = x_0 + alpha_0 p_0
    r_.axpy(-alpha, q);                            // r_1     = r_0 - alpha_0 Ap_0

    if (this->hasIgnore())
        for (size_t i=0; i < r_.size(); ++i)
            if (this->ignore()[i].any())
                r_[i] = 0;

    if (preconditioner_)
        preconditioner_->apply(q, r_);
    else
        q = r_;

    const double r_squared = q * r_;
    const double beta = r_squared / r_squared_old_; // beta_0 = r_1*r_1/ (r_0*r_0)
    p_ *= beta;                                    // p_1 = r_1 + beta_0 p_0
    p_ += q;
    r_squared_old_ = r_squared;
}
