// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_CGSTEP_HH

#include <dune/solvers/common/preconditioner.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>

namespace Dune {
    namespace Solvers {

        //! A conjugate gradient solver
        template <class MatrixType, class VectorType>
        class CGStep : public IterationStep<VectorType>
        {
            using Base = IterationStep<VectorType>;

        public:
            CGStep()
                : preconditioner_(nullptr)
            {}

            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs)
                : Base(x), p_(rhs.size()), r_(rhs), matrix_(stackobject_to_shared_ptr(matrix)),
                  preconditioner_(nullptr)
            {}

            CGStep(const MatrixType& matrix,
                   VectorType& x,
                   const VectorType& rhs,
                   Preconditioner<MatrixType, VectorType>& preconditioner)
                : Base(x), p_(rhs.size()), r_(rhs), matrix_(stackobject_to_shared_ptr(matrix)),
                  preconditioner_(&preconditioner)
            {}

            //! Set linear operator, solution and right hand side
            virtual void setProblem(const MatrixType& mat, VectorType& x, const VectorType& rhs)
            {
                matrix_ = stackobject_to_shared_ptr(mat);
                setProblem(x);
                r_ = rhs;
            }

            void check() const;
            void preprocess();
            void iterate();

        private:
            VectorType p_; // search direction
            VectorType r_; // residual
            using Base::x_;
            std::shared_ptr<const MatrixType> matrix_;
            double r_squared_old_;
            Preconditioner<MatrixType, VectorType>* preconditioner_;

            using Base::setProblem;
        };


#include "cgstep.cc"
    }
}

#endif
