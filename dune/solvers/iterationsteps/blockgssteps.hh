#ifndef DUNE_SOLVERS_ITERATIONSTEPS_BLOCKGSSTEPS_HH
#define DUNE_SOLVERS_ITERATIONSTEPS_BLOCKGSSTEPS_HH

#include <cassert>
#include <functional>

#include <dune/common/parametertree.hh>

#include <dune/matrix-vector/ldlt.hh>

#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/iterationsteps/lineariterationstep.hh>

namespace Dune {
namespace Solvers {

namespace BlockGS {

enum class Direction { FORWARD, BACKWARD, SYMMETRIC };

/**
 * @brief Provides support to parse the @Direction enum.
 */
std::istream& operator>>(std::istream& lhs, Direction& t);

/**
 * \brief Iterates over all rows i and updates the i-th component by solving
 * the i-th correction problem by means of localSolver.
 * \param m Matrix
 * \param x Solution vector
 * \param b Inhomogeneity
 * \param ignore Ignored DOFs
 * \param localSolver
 * \param direction Iteration direction (e.g. forward, backward, ...)
 */
template <class M, class V, class BitVector, class LocalSolver>
void linearStep(const M& m, V& x, const V& b, const BitVector* ignore,
                LocalSolver&& localSolver,
                Direction direction = Direction::FORWARD) {
  // Note: move-capture requires C++14
  auto blockStep = [&, localSolver = std::move(localSolver) ](size_t i) {
    const auto& row_i = m[i];

    // Compute residual
    auto ri = b[i];
    using Block = typename M::block_type;
    const Block* diag = nullptr;
    for (auto cIt = row_i.begin(); cIt != row_i.end(); ++cIt) {
      size_t j = cIt.index();
      cIt->mmv(x[j], ri);
      if (j == i)
        diag = &*cIt;
    }

    using Ignore = std::bitset<V::block_type::dimension>;

    // Update iterate with correction
    x[i] += localSolver(diag != nullptr ? *diag : Block(0.0), std::move(ri),
                        ignore != nullptr ? (*ignore)[i] : Ignore(0));
  };

  if (direction != Direction::BACKWARD)
    for (size_t i = 0; i < x.size(); ++i)
      blockStep(i);
  if (direction != Direction::FORWARD)
    for (size_t i = x.size(); i > 0; --i)
      blockStep(i - 1);
}

/// \brief Convenience method for LinearIterationSteps.
template <class M, class V, class BitVector, class LocalSolver>
void linearStep(LinearIterationStep<M, V, BitVector>& lis,
                LocalSolver&& localSolver,
                Direction direction = Direction::FORWARD) {
  BitVector const* ignore = lis.hasIgnore() ? &lis.ignore() : nullptr;
  linearStep(*lis.mat_, *lis.x_, *lis.rhs_, ignore,
             std::forward<LocalSolver>(localSolver), direction);
}

/**
 * \brief All functions in this namespace are assumed to compute the solution
 *        to a correction problem. Hence they do not require an initial guess
 *        (and assume it to be zero) and return the (approximate) solution.
 */
namespace LocalSolvers {

namespace LinearSolvers {

/** \brief Solves a block problem using the MBlock-inherent solve method. See
 * e.g. FieldMatrix::solve.
 */
template <class MBlock, class VBlock>
VBlock direct(const MBlock& m, const VBlock& b) {
  VBlock x;
  m.solve(x, b);
  return x;
}

//! \brief Solves a block problem using LDL^t decomposition.
template <class MBlock, class VBlock>
VBlock ldlt(MBlock m, const VBlock& b) {
  VBlock x;
  Dune::MatrixVector::ldlt(m, m, m);
  Dune::MatrixVector::ldltSolve(m, m, b, x);
  return x;
}

static constexpr size_t defaultCgMaxIter = 100;
static constexpr double defaultCgTol = 1e-15;

//! \brief Solves a block problem using the conjugate gradient method.
template <class MBlock, class VBlock>
VBlock cg(const MBlock& m, VBlock r, size_t maxIter = defaultCgMaxIter,
          double tol = defaultCgTol) {
  VBlock x(0);
  VBlock p = r;
  auto q = r;
  auto rOldSquared = r * r;
  auto tolSquared = tol * tol;

  for (size_t j = 0; j < maxIter; ++j) {
    m.mv(p, q);
    if (rOldSquared < tolSquared)
      break;
    auto alpha = rOldSquared / (q * p);
    x.axpy(alpha, p);
    r.axpy(-alpha, q);
    auto rSquared = r * r;
    auto beta = rSquared / rOldSquared;
    p *= beta;
    p += r;
    rOldSquared = rSquared;
  }
  return x;
}

static constexpr double defaultGsTol = 0.0;

/**
 * \brief Solves a block problem using a single Gauss--Seidel iteration.
 * \param tol Lines with diagonal entries below this value are skipped and
 * their respective solutions are set to 0.
 */
template <class MBlock, class VBlock>
VBlock gs(const MBlock& m, const VBlock& b, double tol = defaultGsTol) {
  VBlock x(0);
  for (size_t i = 0; i < m.N(); ++i) {
    const auto& mi = m[i];
    const auto& mii = mi[i];
    if (std::abs(mii) <= tol)
      continue;
    x[i] = b[i];
    const auto& end = mi.end();
    for (auto it = mi.begin(); it != end; ++it) {
      auto j = it.index();
      if (j != i)
        x[i] -= (*it) * x[j];
    }
    x[i] /= mii;
  }
  return x;
}

} // end namespace LinearSolvers

namespace LocalSolverFromLinearSolver {
// Note: move-capture, auto-return and auto-lambda-arguments require C++14
/**
 * \brief Applies truncation to the block problem according to the local ignore
 * nodes. The system is modified in such a way that for nodes that are to be
 * ignored, the corresponding rhs is set to 0 while the corresponding matrix row
 * is set to be the appropriate euclidean basis vector, essentially enforcing an
 * exact solution for this node to be zero. This leads to zero correction
 * contributions for the ignored DOFs.
 * \param ignore The global ignore nodes where the local ignore nodes can be
 * obtained from.
 * \param localSolver The block solver used to solve the (modified) local
 * problem.
 */
template <class LinearSolver>
auto truncateSymmetrically(LinearSolver&& linearSolver) {
  return [linearSolver = std::move(linearSolver) ](
      const auto& m, const auto& b, const auto& ignore) {
    using Return = typename std::result_of<LinearSolver(decltype(m), decltype(b))>::type;
    if (ignore.all())
      return Return(0);

    if (ignore.none())
      return linearSolver(m, b);

    auto mTruncated = m;
    auto bTruncated = b;
    assert(b.size() == m.N());
    assert(m.N() == m.M());
    size_t blockSize = b.size();
    for (size_t j = 0; j < blockSize; ++j) {
      if (not ignore[j])
        continue;
      bTruncated[j] = 0;
      for (size_t k = 0; k < blockSize; ++k)
        mTruncated[j][k] = mTruncated[k][j] = (k == j);
    }
    return linearSolver(std::move(mTruncated), std::move(bTruncated));
  };
}

} // end namespace LocalSolverFromLinearSolver

namespace LocalSolverRegularizer {

static constexpr double defaultDiagRegularizeParameter = 1e-10;

// Note: move-capture, auto-return and auto-lambda-arguments require C++14
/**
 * \brief Applies regularization to the diagonal of the block problem, to
 *        render possibly indefinite problems definite (and hence enable the
 *        application of more block problem solve techniques).
 * \param p Diagonal entries that are non-zero are scaled by (1+p) while
 *        zero diagonal entries are set to be p.
 */
template <class LocalSolver>
auto diagRegularize(double p, LocalSolver&& localSolver) {
  return [ p, localSolver = std::move(localSolver) ](
      const auto& m, const auto& b, const auto& ignore) {
    if (p == 0)
      return localSolver(m, b, ignore);
    auto m_regularized = m;
    for (size_t j = 0; j < m_regularized.N(); ++j)
      if (!ignore[j]) {
        if (m_regularized[j][j] == 0)
          m_regularized[j][j] = p;
        else
          m_regularized[j][j] *= 1.0 + p;
      }
    return localSolver(std::move(m_regularized), b, ignore);
  };
}

} // end namespace LocalSolverRegularizer

//! \brief is @LinearSolvers::direct with ignore nodes.
//! \param r Regularization parameter. Set to 0.0 to switch off regularization.
template <typename dummy = void>
auto direct(double r = LocalSolverRegularizer::defaultDiagRegularizeParameter) {
  return [r](const auto& m, const auto& b, const auto& ignore) {
    auto directSolver = LinearSolvers::direct<std::decay_t<decltype(m)>,
                                              std::decay_t<decltype(b)>>;
    auto directTruncatedSolver =
        LocalSolverFromLinearSolver::truncateSymmetrically(directSolver);
    auto directTruncatedRegularizedSolver =
        LocalSolverRegularizer::diagRegularize(r, directTruncatedSolver);
    return directTruncatedRegularizedSolver(m, b, ignore);
  };
}

//! \brief is @LinearSolvers::ldlt with ignore nodes.
template <typename dummy = void>
auto ldlt(double r = LocalSolverRegularizer::defaultDiagRegularizeParameter) {
  return [r](const auto& m, const auto& b, const auto& ignore) {
    auto ldltSolver = LinearSolvers::ldlt<std::decay_t<decltype(m)>,
                                          std::decay_t<decltype(b)>>;
    auto ldltTruncatedSolver =
        LocalSolverFromLinearSolver::truncateSymmetrically(ldltSolver);
    auto ldltTruncatedRegularizedSolver =
        LocalSolverRegularizer::diagRegularize(r, ldltTruncatedSolver);
    return ldltTruncatedRegularizedSolver(m, b, ignore);
  };
}

//! \brief is @LinearSolvers::cg with ignore nodes.
template <typename dummy = void>
auto cg(size_t maxIter = LinearSolvers::defaultCgMaxIter,
        double tol = LinearSolvers::defaultCgTol,
        double r = LocalSolverRegularizer::defaultDiagRegularizeParameter) {
  return [maxIter, tol, r](const auto& m, const auto& b, const auto& ignore) {
    using namespace std::placeholders;
    auto cgSolver = std::bind(
        LinearSolvers::cg<std::decay_t<decltype(m)>, std::decay_t<decltype(b)>>,
        _1, _2, maxIter, tol);
    auto cgTruncatedSolver = LocalSolverFromLinearSolver::truncateSymmetrically(cgSolver);
    auto cgTruncatedRegularizedSolver =
        LocalSolverRegularizer::diagRegularize(r, cgTruncatedSolver);
    return cgTruncatedRegularizedSolver(m, b, ignore);
  };
}

/**
 * \brief is @LinearSolvers::gs with ignore nodes.
 * \param tol Tolerance value for skipping potentially zero diagonal entries
 * after regularization.
 * \param r Regularization parameter. Set to 0.0 to switch off regularization.
 */
template <typename dummy = void>
auto gs(double tol = LinearSolvers::defaultGsTol,
        double r = LocalSolverRegularizer::defaultDiagRegularizeParameter) {
  return [tol, r](const auto& m, const auto& b, const auto& ignore) {
    using namespace std::placeholders;
    auto gsSolver = std::bind(
        LinearSolvers::gs<std::decay_t<decltype(m)>, std::decay_t<decltype(b)>>,
        _1, _2, tol);
    auto gsTruncatedSolver =
        LocalSolverFromLinearSolver::truncateSymmetrically(gsSolver);
    auto gsTruncatedRegularizedSolver =
        LocalSolverRegularizer::diagRegularize(r, gsTruncatedSolver);
    return gsTruncatedRegularizedSolver(m, b, ignore);
  };
}

} // end namespace LocalSolvers

} // end namespace BlockGS

/**
 * \brief A Gauss--Seidel-type linear iteration step.
 * \param localSolver The solver for the linear block correction problems.
 */
template <class Matrix, class Vector, class BitVector, class LocalSolver>
struct BlockGSStep : public LinearIterationStep<Matrix, Vector, BitVector> {

  template<class LS>
  BlockGSStep(LS&& localSolver,
              BlockGS::Direction direction = BlockGS::Direction::FORWARD)
      : localSolver_(std::forward<LS>(localSolver))
      , direction_(direction) {}

  void iterate() {
    assert(this->mat_ != nullptr);
    assert(this->x_ != nullptr);
    assert(this->rhs_ != nullptr);
    BlockGS::linearStep(*this, localSolver_, direction_);
  }

private:
  LocalSolver localSolver_;
  BlockGS::Direction direction_;
};

/**
 * @brief The Type enum provides representatives for (new) block Gauss-Seidel
 *        steps that can be constructed via the @BlockGSStepFactory.
 */
enum class BlockGSType { Direct, LDLt, CG, GS };

/**
 * @brief Provides support to parse the @BlockGSType enum.
 */
std::istream& operator>>(std::istream& lhs, BlockGSType& t);

/**
 * @brief The BlockGSStepFactory struct allows to conveniently construct various
 *        (new) block Gauss-Seidel steps through a ParameterTree config.
 */
template <class Matrix, class Vector,
          class BitVector = DefaultBitVector_t<Vector>>
struct BlockGSStepFactory {
  using BlockGSStepPtr =
      std::shared_ptr<LinearIterationStep<Matrix, Vector, BitVector>>;

  // TODO add ProjectedBlockGSStep (not linear!)
  // TODO add CachedLDLtBlockGSStep

  //! \brief Create a BlockGSStep with custom local solver.
  template <class LocalSolver>
  static auto create(
      LocalSolver&& localSolver,
      BlockGS::Direction direction = BlockGS::Direction::FORWARD) {
    return BlockGSStep<Matrix, Vector, BitVector, LocalSolver>(
        std::forward<LocalSolver>(localSolver), direction);
  }

  template <class LocalSolver>
  static auto createPtr(
      LocalSolver&& localSolver,
      BlockGS::Direction direction = BlockGS::Direction::FORWARD) {
    return std::make_shared<
        BlockGSStep<Matrix, Vector, BitVector, LocalSolver>>(
        std::forward<LocalSolver>(localSolver), direction);
  }

  static BlockGSStepPtr createPtrFromConfig(const Dune::ParameterTree& config) {
    auto type = config.get<BlockGSType>("type");
    auto dir = config.get<BlockGS::Direction>("direction",
                                              BlockGS::Direction::FORWARD);
    auto reg = config.get("regularize_diag",
                          BlockGS::LocalSolvers::LocalSolverRegularizer::
                              defaultDiagRegularizeParameter);
    switch (type) {
    case BlockGSType::Direct:
      return createPtr(BlockGS::LocalSolvers::direct(reg), dir);
    case BlockGSType::LDLt:
      return createPtr(BlockGS::LocalSolvers::ldlt(reg), dir);
    case BlockGSType::CG: {
      auto maxIter = config.get(
          "maxIter", BlockGS::LocalSolvers::LinearSolvers::defaultCgMaxIter);
      auto tol =
          config.get("tol", BlockGS::LocalSolvers::LinearSolvers::defaultCgTol);
      return createPtr(BlockGS::LocalSolvers::cg(maxIter, tol, reg), dir);
    }
    case BlockGSType::GS: {
      auto tol =
          config.get("tol", BlockGS::LocalSolvers::LinearSolvers::defaultGsTol);
      return createPtr(BlockGS::LocalSolvers::gs(tol, reg), dir);
    }
    default:
      DUNE_THROW(Dune::NotImplemented,
                 "Factory cannot create this BlockGSType.");
    }
  }
};

} // end namespace Solvers
} // end namespace Dune

#endif // DUNE_SOLVERS_ITERATIONSTEPS_BLOCKGSSTEPS_HH
