// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_ITERATIVE_SOLVER_HH
#define DUNE_SOLVERS_ITERATIVE_SOLVER_HH

#include <dune/common/ftraits.hh>

#include <dune/solvers/common/defaultbitvector.hh>
#include <dune/solvers/solvers/solver.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>
#include <dune/solvers/norms/norm.hh>

namespace Dune {

  namespace Solvers {

    /** \brief Abstract base class for iterative solvers */
    template <class VectorType, class BitVectorType = DefaultBitVector_t<VectorType> >
    class IterativeSolver : public Solver
    {
        // For norms and convergence rates
        typedef typename Dune::FieldTraits<VectorType>::real_type real_type;

    public:

        /** \brief Constructor taking all relevant data */
        IterativeSolver(double tolerance,
                        int maxIterations,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations), errorNorm_(NULL),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Constructor taking all relevant data */
        IterativeSolver(int maxIterations,
                        double tolerance,
                        const Norm<VectorType>* errorNorm,
                        VerbosityMode verbosity,
                        bool useRelativeError=true)
            : Solver(verbosity),
              tolerance_(tolerance),
              maxIterations_(maxIterations), errorNorm_(errorNorm),
              historyBuffer_(""),
              useRelativeError_(useRelativeError)
        {}

        /** \brief Loop, call the iteration procedure
         * and monitor convergence */
        virtual void solve() = 0;

        /** \brief Checks whether all relevant member variables are set
         */
        virtual void check() const;

        /** \brief Write the current iterate to disk (for convergence measurements) */
        void writeIterate(const VectorType& iterate, int iterationNumber) const;

        //! The iteration step used by the algorithm
        IterationStep<VectorType, BitVectorType>* iterationStep_;

        /** \brief The requested tolerance of the solver */
        double tolerance_;

        //! The maximum number of iterations
        int maxIterations_;

        //! The norm used to measure convergence
        const Norm<VectorType>* errorNorm_;

        /** \brief If this string is nonempty it is expected to contain a valid
            directory name.  All intermediate iterates are then written there. */
        std::string historyBuffer_;

        real_type maxTotalConvRate_;

        bool useRelativeError_;

    };

  }   // namespace Solvers

}   // namespace Dune

// For backward compatibility: will be removed eventually
using Dune::Solvers::IterativeSolver;

#include "iterativesolver.cc"

#endif
