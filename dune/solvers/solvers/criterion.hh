// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=8 sw=2 sts=2:
#ifndef DUNE_SOLVERS_SOLVERS_CRITERION_HH
#define DUNE_SOLVERS_SOLVERS_CRITERION_HH


#include <string>
#include <tuple>
#include <memory>
#include <type_traits>

#include <dune/common/stringutility.hh>


/**
 * \attention Everything contained in this headere is experimental and may change in the near future.
 */

namespace Dune {

  namespace Solvers {

    /**
     * \brief Class to wrap termination criteria
     *
     * Each criterion is a functor returning a std::tuple<bool,string>
     * where the bool encodes if the criterion is satisfied whereas
     * the string will be written to the logging output.
     */
    class Criterion
    {
      typedef std::tuple<bool, std::string> Result;

    public:

      /**
       * \brief Create Criterion from function and header string
       *
       * \param f A functor returning a tuple<bool,string>
       * \param header The header string describing the output of this criterion
       *
       * The bool of the tuple returned by the fuctor is interpreted
       * as 'criterion satisfied' whereas the string will be written to
       * the log output.
       *
       * Be aware that this stores a copy of the provided functor.
       * Hence it should be lightweight and avoid holding large
       * data sets or wrapped using std::cref.
       *
       * Take care that the length of returned strings and the length
       * of the header string coincide to get a readable log.
       */
      template<class F,
        std::enable_if_t<std::is_convertible<std::result_of_t<F()>, Result>::value, int> = 0>
      Criterion(F&& f, const std::string& header) :
        f_(std::forward<F>(f)),
        header_(header)
      {}

      /**
       * \brief Create always false Criterion for logging strings
       *
       * \param f A functor returning a string
       * \param header The header string describing the output of this criterion
       *
       * The string returned by the functor will be written to
       * the log output while criterion is interpreted as always
       * false.
       *
       * Be aware that this stores a copy of the provided functor.
       * Hence it should be lightweight and avoid holding large
       * data sets or wrapped using std::cref.
       *
       * Take care that the length of returned strings and the length
       * of the header string coincide to get a readable log.
       */
      template<class F,
        std::enable_if_t<std::is_convertible<std::result_of_t<F()>, std::string>::value, int> = 0>
      Criterion(F&& f, const std::string& header) :
        f_( [=]() {return std::make_tuple(false, f());} ),
        header_(header)
      {}

      /**
       * \brief Create always false Criterion for logging any printf-like type
       *
       * \param f A functor returning some type
       * \param format A printf format string
       * \param header The header string describing the output of this criterion
       *
       * The valued returned by the functor will be converted using the
       * format string and written to the log output while criterion is
       * interpreted as always false.
       *
       * Be aware that this stores a copy of the provided functor.
       * Hence it should be lightweight and avoid holding large
       * data sets or wrapped using std::cref.
       *
       * Take care that the length of returned strings and the length
       * of the header string coincide to get a readable log.
       */
      template<class F>
      Criterion(F&& f, const std::string& format, const std::string& header) :
        f_( [=]() {return std::make_tuple(false, formatString(format, f()));} ),
        header_(header)
      {}

      /**
       * \brief Check criterion
       *
       * \returns A tuple<bool,string>
       */
      Result operator()() const;

      /**
       * \brief Obtain header string
       */
      std::string header() const;

    protected:
      std::function<Result()> f_;
      std::string header_;
    }; // end of class Criterion



    // free helper functions for logical operations on criteria

    /**
     * \brief Create Criterion that combines two others using 'or'
     *
     * The log output of both criteria will be concatenated.
     *
     * Notice that this will store copies of both provided criteria.
     */
    Criterion operator| (const Criterion& c1, const Criterion& c2);

    /**
     * \brief Create Criterion that combines two others using 'and'
     *
     * The log output of both criteria will be concatenated.
     *
     * Notice that this will store copies of both provided criteria.
     */
    Criterion operator& (const Criterion& c1, const Criterion& c2);

    /**
     * \brief Create Criterion that negates another
     *
     * The log output will be forwarded.
     *
     * Notice that this will store a copy of the provided criterion.
     */
    Criterion operator~ (const Criterion& c);




    // free helper functions for generating some classic criteria

    /**
     * \brief Create a Criterion for limiting the number of iteration steps
     *
     * \param solver The criterion will query this solver for its current iteration step.
     * \param maxIterations The criterion will be true once this number of iteration steps is reached.
     */
    template<class SolverType>
    Criterion maxIterCriterion(const SolverType& solver, int maxIterations)
    {
        return Criterion(
                [&solver, maxIterations](){
                    int i = solver.iterationCount();
                    return std::make_tuple( i>=maxIterations, Dune::formatString("% 7d", i) );
                },
                "   iter");
    }

    /**
     * \brief Create a Criterion for checking the correction norm
     *
     * \param iterationStep The IterationStep to be monitored
     * \param norm          Norm to be evaluated for the correction
     * \param tolerance     Tolerance necessary to satisfy the criterion
     * \param correctionNorms A container to store the computed correction norms
     */
    template<class IterationStepType, class NormType, class CorrectionNormContainer>
    Criterion correctionNormCriterion(
          const IterationStepType& iterationStep,
          const NormType& norm,
          double tolerance,
          CorrectionNormContainer& correctionNorms)
    {
      auto lastIterate = std::make_shared<typename IterationStepType::Vector>(*iterationStep.getIterate());
      return Criterion(
          [&, lastIterate, tolerance] () mutable {
            double normOfCorrection = norm.diff(*lastIterate, *iterationStep.getIterate());
            correctionNorms.push_back(normOfCorrection);
            *lastIterate = *iterationStep.getIterate();
            return std::make_tuple(normOfCorrection < tolerance, Dune::formatString(" % 14.7e", normOfCorrection));
          },
          " abs correction");
    }

    /**
     * \brief Create a Criterion for checking the correction norm
     *
     * \param iterationStep The IterationStep to be monitored
     * \param norm          Norm to be evaluated for the correction
     * \param tolerance     Tolerance necessary to satisfy the criterion
     */
    template<class IterationStepType, class NormType>
    Criterion correctionNormCriterion(
          const IterationStepType& iterationStep,
          const NormType& norm,
          double tolerance)
    {
      auto lastIterate = std::make_shared<typename IterationStepType::Vector>(*iterationStep.getIterate());
      return Criterion(
          [&, lastIterate, tolerance] () mutable {
            double normOfCorrection = norm.diff(*lastIterate, *iterationStep.getIterate());
            *lastIterate = *iterationStep.getIterate();
            return std::make_tuple(normOfCorrection < tolerance, Dune::formatString(" % 14.7e", normOfCorrection));
          },
          " abs correction");
    }

  } // end namespace Solvers

} // end namespace Dune


#endif


