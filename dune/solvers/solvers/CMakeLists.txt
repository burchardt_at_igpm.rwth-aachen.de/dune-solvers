install(FILES
    criterion.hh
    iterativesolver.cc
    iterativesolver.hh
    linearipopt.hh
    loopsolver.cc
    loopsolver.hh
    quadraticipopt.hh
    solver.hh
    tcgsolver.cc
    tcgsolver.hh
    trustregionsolver.cc
    trustregionsolver.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/solvers/solvers)
