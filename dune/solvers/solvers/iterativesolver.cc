// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <cmath>
#include <limits>
#include <iostream>
#include <iomanip>
#include <fstream>

#include <dune/solvers/common/genericvectortools.hh>

template <class VectorType, class BitVectorType>
void IterativeSolver<VectorType, BitVectorType>::check() const
{
    if (!iterationStep_)
        DUNE_THROW(SolverError, "You need to supply an iteration step to an iterative solver!");

    iterationStep_->check();

    if (!errorNorm_)
        DUNE_THROW(SolverError, "You need to supply a norm-computing routine to an iterative solver!");
}

template <class VectorType, class BitVectorType>
void IterativeSolver<VectorType, BitVectorType>::writeIterate(const VectorType& iterate,
                                                              int iterationNumber) const
{
    std::stringstream iSolFilename;
    iSolFilename << historyBuffer_ << "/intermediatesolution_" 
                 << std::setw(4) << std::setfill('0') << iterationNumber;
    
    std::ofstream file(iSolFilename.str().c_str(), std::ios::out|std::ios::binary);
    if (not(file))
        DUNE_THROW(SolverError, "Couldn't open file " << iSolFilename.str() << " for writing");
    
    GenericVector::writeBinary(file, iterate);
    
    file.close();
}
