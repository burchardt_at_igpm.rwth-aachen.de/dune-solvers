// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_SOLVERS_SOLVER_HH
#define DUNE_SOLVERS_SOLVER_HH

#include <dune/solvers/common/numproc.hh>

/**  \brief struct to store result related information such as for example number of iterations etc.
  *
  *  \warning The interface and implementation is so far highly experimental and will change without further notice!
  */
struct SolverResult
{
    size_t iterations;
    bool converged;
    double conv_rate;
//  double reduction;
//  double elapsed;
    SolverResult(){}

    SolverResult(size_t iter, bool conv, double rate):
        iterations(iter),
        converged(conv),
        conv_rate(rate)
    {}

};

/** \brief The base class for all sorts of solver algorithms */
class Solver : public NumProc
{
    public:
        Solver(VerbosityMode verbosity=FULL)
            : NumProc(verbosity)
        {}

        /** \brief Virtual destructor */
        virtual ~Solver() {}

        /** \brief Do the necessary preprocessing */
        virtual void preprocess()
        {
            // Default: Do nothing
        }

        /** \brief Derived classes overload this with the actual
         * solution algorithm */
        virtual void solve() = 0;

        SolverResult& getResult()
        {
            return result;
        }

        void setResult(const SolverResult& new_result)
        {
            result = new_result;
        }

        void setResult(size_t iter, bool conv, double rate)
        {
            result.iterations = iter;
            result.converged = conv;
            result.conv_rate = rate;
        }

    private:
        SolverResult result;
};
#endif
