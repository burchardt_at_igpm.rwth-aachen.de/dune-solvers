// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef LOOP_SOLVER_HH
#define LOOP_SOLVER_HH

#include <dune/solvers/solvers/iterativesolver.hh>
#include <dune/solvers/iterationsteps/iterationstep.hh>
#include <dune/solvers/norms/norm.hh>
#include <dune/solvers/solvers/criterion.hh>
#include <dune/solvers/common/defaultbitvector.hh>


namespace Dune {

namespace Solvers {

/** \brief A solver which consists of a single loop
  *
  *  This class basically implements a loop that calls an iteration procedure
  *  (which is to be supplied by the user).  It also monitors convergence.
  */
template <class VectorType, class BitVectorType = Dune::Solvers::DefaultBitVector_t<VectorType> >
class LoopSolver : public IterativeSolver<VectorType, BitVectorType>
{
    typedef typename VectorType::field_type field_type;

    // For complex-valued data
    typedef typename Dune::FieldTraits<field_type>::real_type real_type;

public:

    /** \brief Constructor taking all relevant data */
    LoopSolver(IterationStep<VectorType, BitVectorType>* iterationStep,
               int maxIterations,
               double tolerance,
               const Norm<VectorType>* errorNorm,
               Solver::VerbosityMode verbosity,
               bool useRelativeError=true,
               const VectorType* referenceSolution=0)
        : IterativeSolver<VectorType, BitVectorType>(maxIterations,
                                                     tolerance,
                                                     errorNorm,
                                                     verbosity,
                                                     useRelativeError),
          referenceSolution_(referenceSolution)
    {
        this->iterationStep_ = iterationStep;
    }

    /**
     * \brief Add a convergence criterion
     *
     * All criteria are checked after each loop. The solver loop will
     * terminate once any of the supplied criteria evaluates to true.
     * If you want to terminate only if several criteria are true
     * you can combine criteria using the overloaded bitwise
     * logical operations.
     *
     * Besides checking the criterion the LoopSolver will
     * print the diagnostic string returned by the criterion
     * in a column with the criterions header string on top.
     *
     * This method forward all arguments to the constructor of
     * Criterion. So you can pass a Criterion or any argument
     * list supported by the Criterion constructors.
     *
     * Notice, that the old hard-wired criteria are still active.
     * If you don't supply any criteria here, the you will get
     * exactly the old behaviour.
     *
     * \attention This is an experimental feature that may still change in the near future.
     */
    template<class... Args>
    void addCriterion(Args&&... args)
    {
        criteria_.emplace_back(std::forward<Args>(args)...);
    }

    /**
     * \brief Get current iteration count
     */
    int iterationCount() const
    {
        return iter_;
    }

    virtual void preprocess();

    /**  \brief Loop, call the iteration procedure
      *  and monitor convergence
      */
    virtual void solve();

    const VectorType* referenceSolution_;
protected:
    std::vector<Dune::Solvers::Criterion> criteria_;

    int iter_;
};

}  // namespace Solvers

}  // namespace Dune

// For backward compatibility: will be removed eventually

using Dune::Solvers::LoopSolver;

#include "loopsolver.cc"

#endif
