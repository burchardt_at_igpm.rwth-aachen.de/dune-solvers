// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef DUNE_NUMPROC_HH
#define DUNE_NUMPROC_HH

#include <dune/common/exceptions.hh>

/** \brief Exception thrown by solvers */
class SolverError : public Dune::Exception {};

    /** \brief Base class for numerical procedures */
    class NumProc
    {
    public:

        /** \brief Different levels of verbosity */
        enum VerbosityMode {QUIET, REDUCED, FULL};

        NumProc() : verbosity_(FULL) {}

        NumProc(VerbosityMode verbosity) 
            : verbosity_(verbosity)
        {}

        /** \brief Set the verbosity level */
        void setVerbosity(VerbosityMode verbosity) {verbosity_ = verbosity;}

        /** \brief Controls the amount of screen output of a numproc */
        VerbosityMode verbosity_;

    };

    inline
    std::istream& operator>>(std::istream &lhs, NumProc::VerbosityMode &e)
    {
        std::string s;
        lhs >> s;

        if (s == "full" || s == "2")
            e = NumProc::FULL;
        else if (s == "reduced" || s == "1")
            e = NumProc::REDUCED;
        else if (s == "quiet" || s == "0")
            e = NumProc::QUIET;
        else
            lhs.setstate(std::ios_base::failbit);

        return lhs;
    }



#endif
