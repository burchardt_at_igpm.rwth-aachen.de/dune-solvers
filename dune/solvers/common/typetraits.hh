#ifndef DUNE_SOLVERS_COMMON_TYPETRAITS_HH
#define DUNE_SOLVERS_COMMON_TYPETRAITS_HH

#include <type_traits>

namespace Dune {
namespace Solvers {
  template <typename T>
  struct IsNumber
      : public std::integral_constant<bool, std::is_arithmetic<T>::value> {};

  template <typename T>
  struct IsNumber<std::complex<T>>
      : public std::integral_constant<bool, IsNumber<T>::value> {};
}
}

#endif
