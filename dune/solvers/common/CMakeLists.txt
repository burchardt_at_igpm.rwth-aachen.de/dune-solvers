install(FILES
    algorithm.hh
    arithmetic.hh
    boxconstraint.hh
    canignore.hh
    copyorreference.hh
    defaultbitvector.hh
    genericvectortools.hh
    interval.hh
    numproc.hh
    permutationmanager.hh
    preconditioner.hh
    resize.hh
    staticmatrixtools.hh
    tuplevector.hh
    typetraits.hh
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/solvers/common)
