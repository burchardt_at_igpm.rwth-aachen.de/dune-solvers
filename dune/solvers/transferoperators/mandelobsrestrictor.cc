// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <dune/solvers/transferoperators/compressedmultigridtransfer.hh>
#include <dune/solvers/transferoperators/densemultigridtransfer.hh>

template <class VectorType>
void MandelObstacleRestrictor<VectorType>::
restrict(const std::vector<BoxConstraint<field_type, blocksize> >&f,
        std::vector<BoxConstraint<field_type, blocksize> > &t,
        const Dune::BitSetVector<blocksize>& fHasObstacle,
        const Dune::BitSetVector<blocksize>& tHasObstacle,
        const MultigridTransfer<VectorType>& transfer,
        const Dune::BitSetVector<blocksize>& critical)
{

    if (dynamic_cast<const CompressedMultigridTransfer<VectorType>*>(&transfer))
        restrict(f,t,fHasObstacle,tHasObstacle,
            dynamic_cast<const CompressedMultigridTransfer<VectorType>*>(&transfer)->getMatrix(),critical);
    else if (dynamic_cast<const DenseMultigridTransfer<VectorType>*>(&transfer))
        restrict(f,t,fHasObstacle,tHasObstacle,
            dynamic_cast<const DenseMultigridTransfer<VectorType>*>(&transfer)->getMatrix(),critical);
    else
        DUNE_THROW(Dune::NotImplemented, "Implementation assumes transfer to be of type CompressedMultigridTransfer "
                                         "or DenseMultigridTransfer!");
}

template <class VectorType>
template <int dim>
void MandelObstacleRestrictor<VectorType>::
restrict(const std::vector<BoxConstraint<field_type, blocksize> >&f,
         std::vector<BoxConstraint<field_type, blocksize> > &t,
         const Dune::BitSetVector<blocksize>& fHasObstacle,
         const Dune::BitSetVector<blocksize>& tHasObstacle,
         const Dune::BCRSMatrix<Dune::FieldMatrix<field_type,dim,dim> >& transferMat,
         const Dune::BitSetVector<blocksize>& critical)
{
    // dim should be either 1 for the compressed transfer or dim
    assert(dim==blocksize || dim==1);

    t.resize(transferMat.M());
    for (size_t i=0; i<t.size(); i++)
        t[i].clear();

    // We use the multigrid transfer matrix to find all relevant fine grid dofs
    // Loop over all coarse grid dofs
    for (size_t i=0; i<transferMat.N(); i++) {

        if (fHasObstacle[i].none())
            continue;

        const auto& row = transferMat[i];

        // Loop over all fine grid base functions in the support of tObsIt
        for(auto cIt=row.begin(); cIt != row.end(); ++cIt) {

            if (tHasObstacle[cIt.index()].none())
                continue;

            // Sort out spurious entries due to numerical dirt
            // initialise with the first entry to ensure to handle the case dim==1
            std::vector<bool> truncated(blocksize,((*cIt)[0][0]<1e-3));
            for (int j=1; j<dim; j++)
                truncated[j] = ((*cIt)[j][j] < 1e-3);


            for (int j=0; j<blocksize; j++)
                if (not truncated[j] and  not critical[i][j]) {
                    t[cIt.index()].lower(j) = std::max(t[cIt.index()].lower(j), f[i].lower(j));
                    t[cIt.index()].upper(j) = std::min(t[cIt.index()].upper(j), f[i].upper(j));
                }

        }

    }

}
