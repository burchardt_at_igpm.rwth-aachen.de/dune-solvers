// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:
#ifndef SUMOPERATOR_HH
#define SUMOPERATOR_HH

#include <cstddef>

/** \brief represents the sum of two linear operators
 *
 *  The summands may be of arbitrary type. Their names are for historical illustrative reasons where one is a sparse and the other a filled in lowrank matrix
 *  \tparam SparseMatrix the type of one of the summands
 *  \tparam LowRankMatrix the type of the other summand
 */
template <class SparseMatrix, class LowRankMatrix>
class SumOperator
{
    public:
        //! export the summand type
        typedef SparseMatrix SparseMatrixType;
        //! export the summand type
        typedef LowRankMatrix LowRankMatrixType;

        typedef typename SparseMatrixType::field_type field_type;

        //! default constructor - allocates memory for summand operators internally
        SumOperator():
            alpha_(1.0),
            beta_(1.0),
            summands_allocated_internally_(true)
        {
            sparse_matrix_ = new SparseMatrixType;
            lowrank_matrix_ = new LowRankMatrixType;
        }

        //! construct from summands
        SumOperator(double a, SparseMatrixType& A, double b, LowRankMatrixType& M):
            alpha_(a),
            beta_(b),
            sparse_matrix_(&A),
            lowrank_matrix_(&M),
            summands_allocated_internally_(false)
        {}

        //! construct from summands
        SumOperator(SparseMatrixType& A, LowRankMatrixType& M):
            alpha_(1.0),
            beta_(1.0),
            sparse_matrix_(&A),
            lowrank_matrix_(&M),
            summands_allocated_internally_(false)
        {}

        ~SumOperator()
        {
            if (summands_allocated_internally_)
            {
                delete sparse_matrix_;
                delete lowrank_matrix_;
            }
        }

        //! b += (A+M)x
        template <class LVectorType, class RVectorType>
        void umv(const LVectorType& x, RVectorType& b) const
        {
            sparse_matrix_->usmv(alpha_,x,b);
            lowrank_matrix_->usmv(beta_,x,b);
        }

        //! b -= (A+M)x
        template <class LVectorType, class RVectorType>
        void mmv(const LVectorType& x, RVectorType& b) const
        {
            sparse_matrix_->usmv(-alpha_,x,b);
            lowrank_matrix_->usmv(-beta_,x,b);
        }

        //! b = (A+M)x
        template <class LVectorType, class RVectorType>
        void mv(const LVectorType& x, RVectorType& b) const
        {
            b = 0.0;
            sparse_matrix_->usmv(alpha_,x,b);
            lowrank_matrix_->usmv(beta_,x,b);
        }

        //! b += a*(A+M)x
        template <class LVectorType, class RVectorType>
        void usmv(const field_type a, const LVectorType& x, RVectorType& b) const
        {
            sparse_matrix_->usmv(a*alpha_,x,b);
            lowrank_matrix_->usmv(a*beta_,x,b);
        }

        //! return the number of rows
        size_t N() const
        {
            return sparse_matrix_->N();
        }

        //! return the number of columns
        size_t M() const
        {
            return sparse_matrix_->M();
        }

        //! return one summand
        const SparseMatrixType& sparseMatrix() const
        {
            return *sparse_matrix_;
        }

        //! return one summand
        SparseMatrixType& sparseMatrix()
        {
            return *sparse_matrix_;
        }

        //! return "the other" summand
        const LowRankMatrixType& lowRankMatrix() const
        {
            return *lowrank_matrix_;
        }

        //! return "the other" summand
        LowRankMatrixType& lowRankMatrix()
        {
            return *lowrank_matrix_;
        }

        double alpha_,
               beta_;
    private:
        //! one summand
        SparseMatrixType*  sparse_matrix_;
        //! the other summand
        LowRankMatrixType* lowrank_matrix_;
        //! are the summands supplied or stored internally
        const bool summands_allocated_internally_;
};

#endif

