// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/scaledidmatrix.hh>

#include <dune/solvers/operators/nulloperator.hh>

#define BLOCKSIZE 2

// this test tests the NullOperator class for several square block_types.
// The test is mainly there to check if it compiles. It uses all implemented methods of NullOperator and
// checks whether they come up with the expected behaviour.

template <class Mblock_type>
bool check()
{
    size_t n=1000;
    typedef Dune::FieldVector<double, Mblock_type::rows> Vblock_type;
    typedef Dune::BlockVector<Vblock_type> Vector;

    Vector x(n), b(n), null_vec(n);
    for (size_t i = 0; i<n; ++i)
        x[i] = (1.0*rand()) / RAND_MAX;
    b = null_vec = 0.0;

    NullOperator<Mblock_type> null_op;

    null_op.umv(x,b);
    for (size_t i=0; i<b.size(); ++i)
    if (b[i] != null_vec[i])
    {
        std::cout << "NullOperator::umv does not leave second argument unchanged." << std::endl;
        return false;
    }

    null_op.usmv(1.0,x,b);
    for (size_t i=0; i<b.size(); ++i)
    if (b[i] != null_vec[i])
    {
        std::cout << "NullOperator::usmv does not leave second argument unchanged." << std::endl;
        return false;
    }

    b=1.0;
    null_op.mv(x,b);
    for (size_t i=0; i<b.size(); ++i)
    if (b[i] != null_vec[i])
    {
        std::cout << "NullOperator::mv does not set second argument to zero." << std::endl;
        return false;
    }

    Mblock_type zero_block(0);

    {
        const Mblock_type block(null_op[n/2][n/4]);
        if (block != zero_block)
        {
            std::cout << "NullOperator[][] does not yield correct zero block" << std::endl;
            return false;
        }
    }

    {
        const Mblock_type block(null_op.diagonal(n/4));
        if (block != zero_block)
        {
            std::cout << "NullOperator::diagonal does not yield correct zero block" << std::endl;
            return false;
        }
    }

    return true;
}

int main(int argc, char** argv)
{
    Dune::MPIHelper::instance(argc, argv);

    static const int block_size = BLOCKSIZE;

    bool passed;

    passed = check<Dune::ScaledIdentityMatrix<double, block_size> >();
    passed = passed and check<Dune::DiagonalMatrix<double, block_size> >();
    passed = passed and check<Dune::FieldMatrix<double, block_size, block_size> >();

    return passed ? 0 : 1;
}
