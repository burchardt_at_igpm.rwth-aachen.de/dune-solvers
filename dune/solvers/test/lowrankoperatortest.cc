// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <cmath>

#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/diagonalmatrix.hh>
#include <dune/common/parallel/mpihelper.hh>

#include <dune/istl/bvector.hh>
#include <dune/istl/matrix.hh>
#include <dune/istl/scaledidmatrix.hh>
#include <dune/istl/io.hh>

#include <dune/solvers/operators/lowrankoperator.hh>

#define BLOCKSIZE 2

// This tests the LowRankOperator's methods for consistency with corresponding operations of the full matrix
template <class StaticMatrixType>
class MatrixMultiplier
{
    public:
        static StaticMatrixType matXmat(const StaticMatrixType& A, const StaticMatrixType& B)
        {
            DUNE_THROW(Dune::NotImplemented,"MatXmat not implemented for this MatrixType.");
        }
};

template <class field_type, int N>
class MatrixMultiplier<Dune::ScaledIdentityMatrix<field_type,N> >
{
        typedef Dune::ScaledIdentityMatrix<field_type,N> StaticMatrixType;
    public:
        static StaticMatrixType matXmat(const StaticMatrixType& A, const StaticMatrixType& B)
        {
            return StaticMatrixType(A.scalar()*B.scalar());
        }
        static StaticMatrixType matTXmat(const StaticMatrixType& A, const StaticMatrixType& B)
        {
            return matXmat(A,B);
        }
};

template <class field_type, int N>
class MatrixMultiplier<Dune::DiagonalMatrix<field_type,N> >
{
        typedef Dune::DiagonalMatrix<field_type,N> StaticMatrixType;
    public:
        static StaticMatrixType matXmat(const StaticMatrixType& A, const StaticMatrixType& B)
        {
            StaticMatrixType r(A);
            for (size_t i=0; i<N; ++i)
                r.diagonal(i) *= B.diagonal(i);
            return r;
        }
        static StaticMatrixType matTXmat(const StaticMatrixType& A, const StaticMatrixType& B)
        {
            return matXmat(A,B);
        }
};


template <class LowRankFactorType>
bool check()
{

    bool passed = true;
    typedef typename LowRankFactorType::block_type block_type;

    size_t m=3, n=1000;
    typedef Dune::FieldVector<double, block_type::rows> Vblock_type;
    typedef Dune::BlockVector<Vblock_type> Vector;

    Vector x(n), b(n), ref_vec(n);
    for (size_t i = 0; i<n; ++i)
//        x[i] = 1.0;
        x[i] = (1.0*rand()) / RAND_MAX;
    b = ref_vec = 0.0;

    double alpha = (1.0*rand())/RAND_MAX;

    LowRankFactorType lr_factor(m,n);
    std::cout << "Checking LowRankOperator<" << Dune::className(lr_factor) << " >  ...  ";

    // set lowrankfactor to random values
    typedef typename block_type::RowIterator BlockRowIterator;
    typedef typename block_type::ColIterator BlockColIterator;
    for (size_t row=0; row<m; ++row)
        for (size_t col=0; col<n; ++col)
        {
            BlockRowIterator row_end = lr_factor[row][col].end();
            for (BlockRowIterator row_it = lr_factor[row][col].begin(); row_it!=row_end; ++ row_it)
            {
                BlockColIterator col_end = row_it->end();
                for (BlockColIterator col_it = row_it->begin(); col_it!=col_end; ++col_it)
//                    *col_it = col+1;
                    *col_it = (1.0*rand())/RAND_MAX;
            }
        }

    LowRankOperator<LowRankFactorType> lr_op(lr_factor);

    LowRankFactorType full_op(n,n);

    for (size_t i=0; i<n; ++i)
        for (size_t j=0; j<n; ++j)
        {
            if (j>=i)
            {
                full_op[i][j] = 0.0;
                for (size_t k=0; k<m; ++k)
                {
                    full_op[i][j] += MatrixMultiplier<block_type>::matTXmat(lr_factor[k][i], lr_factor[k][j]);
                }
            }
            else
                full_op[i][j] = full_op[j][i];
        }

    full_op.mv(x,ref_vec);

    lr_op.umv(x,b);
    for (size_t i=0; i<b.size(); ++i)
    if ((b[i] - ref_vec[i]).two_norm()/b[i].two_norm()>1e-12)
    {
        std::cout << "LowRankOperator::umv does not yield correct result. Difference = " << (b[i] - ref_vec[i]).two_norm() << std::endl;
        passed = false;
        for (size_t j=0; j<b.size(); ++j)
            std::cout << b[j] << "     "<<ref_vec[j] << std::endl;
        std::cout << std::endl;
        break;
    }

    lr_op.mmv(x,b);
    for (size_t i=0; i<b.size(); ++i)
    if (b[i].two_norm()>1e-12)
    {
        std::cout << "LowRankOperator::mmv does not yield correct result. Difference = " << (b[i] - ref_vec[i]).two_norm() << std::endl;
        passed = false;
        for (size_t j=0; j<b.size(); ++j)
            std::cout << b[j] << std::endl;
        std::cout << std::endl;
        break;
    }

    b=1.0;
    lr_op.mv(x,b);
    for (size_t i=0; i<b.size(); ++i)
    if ((b[i] - ref_vec[i]).two_norm()/b[i].two_norm()>1e-12)
    {
        std::cout << "LowRankOperator::mv does not yield correct result." << std::endl;
        passed = false;
        break;
    }

    full_op.usmv(alpha,x,ref_vec);

    lr_op.usmv(alpha,x,b);
    for (size_t i=0; i<b.size(); ++i)
    if ((b[i] - ref_vec[i]).two_norm()/b[i].two_norm()>1e-12)
    {
        std::cout << "LowRankOperator::usmv does not yield correct result. Difference = " << (b[i] - ref_vec[i]).two_norm() << std::endl;
        passed = false;
        for (size_t j=0; j<b.size(); ++j)
            std::cout << b[j] << "     "<<ref_vec[j] << std::endl;
        std::cout << std::endl;
        break;
    }

    LowRankFactorType lr_factor_reborn = lr_op.lowRankFactor();

    if (passed)
        std::cout << "passed";
    std::cout << std::endl;
    return passed;
}

int main(int argc, char** argv)
{
//    static const int block_size = BLOCKSIZE;

    Dune::MPIHelper::instance(argc, argv);
    bool passed(true);

    passed = check<Dune::Matrix<Dune::ScaledIdentityMatrix<double, 1> > >();
    passed = passed and check<Dune::Matrix<Dune::DiagonalMatrix<double, 1> > >();
    passed = check<Dune::Matrix<Dune::ScaledIdentityMatrix<double, 2> > >();
    passed = passed and check<Dune::Matrix<Dune::DiagonalMatrix<double, 2> > >();
    passed = check<Dune::Matrix<Dune::ScaledIdentityMatrix<double, 3> > >();
    passed = passed and check<Dune::Matrix<Dune::DiagonalMatrix<double, 3> > >();
    passed = check<Dune::Matrix<Dune::ScaledIdentityMatrix<double, 4> > >();
    passed = passed and check<Dune::Matrix<Dune::DiagonalMatrix<double, 4> > >();

    return passed ? 0 : 1;
}
