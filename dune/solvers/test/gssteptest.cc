// -*- tab-width: 8; indent-tabs-mode: nil; c-basic-offset: 4 -*-
// vi: set et ts=8 sw=4 sts=4:

#include <config.h>

#include <iostream>

#include <dune/common/bitsetvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/stringutility.hh>

#include <dune/solvers/solvers/loopsolver.hh>
#include <dune/solvers/iterationsteps/blockgsstep.hh>
#include <dune/solvers/iterationsteps/projectedblockgsstep.hh>
#include <dune/solvers/iterationsteps/blockgssteps.hh>
#include <dune/solvers/iterationsteps/truncatedblockgsstep.hh>

#include "common.hh"

/**  \brief test for the GSStep classes
  *
  *  This test tests if the Dirichlet problem for a Laplace operator
  *  is solved correctly for a random rhs by a LoopSolver employing
  *  a GSStep.
  */
template <size_t blocksize, bool trivialDirichletOnly = true>
struct GSTestSuite {
  template <class GridType>
  bool check(const GridType& grid) {
    bool passed = true;

    using Problem = SymmetricSampleProblem<blocksize, typename GridType::LevelGridView,
                                           trivialDirichletOnly>;
    Problem p(grid.levelGridView(grid.maxLevel()));

    const auto verbosity = Solver::QUIET;
    const bool relativeErrors = false;

    using Matrix = typename Problem::Matrix;
    using Vector = typename Problem::Vector;
    enum { BlockSize = Vector::block_type::dimension };

    // create some obstacle
    size_t size = p.u.size();
    std::vector<BoxConstraint<double, BlockSize>> obstacles(size);
    for (size_t i = 0; i < size; ++i)
      for (size_t j = 0; j < BlockSize; ++j)
          obstacles[i][j] = { p.u_ex[i][j] + 2.0, p.u_ex[i][j] + 3.0 };

    using LoopSolver = ::LoopSolver<Vector>;
    using Step = LinearIterationStep<Matrix, Vector>;

    auto solve = [&](Step* step, Vector u_init, double stepTol, size_t maxIterations) {
      step->setProblem(p.A, u_init, p.rhs);
      step->setIgnore(p.ignore);

      LoopSolver solver(step, maxIterations, stepTol, &p.energyNorm, verbosity,
                        relativeErrors);
      solver.check();
      solver.preprocess();
      solver.solve();

      return u_init;
    };

    auto analyse =
        [&](const Vector& v, std::string testCase, double tolerance) {
      const auto normDiff = p.energyNorm.diff(p.u_ex, v);
      const std::string s = Dune::formatString("%*s", -60, testCase.c_str());
      std::cout << "error for solver \"" << s << "\": " << normDiff
                << std::endl;
      if (normDiff > tolerance) {
        std::cerr << "### Error too large for solver \"" << testCase << "\"."
                  << std::endl;
        return false;
      }
      return true;
    };

    auto test = [&](Step* step, std::string name) {
      auto result = solve(step, p.u, 1e-14, 2000);
      passed &= analyse(result, name, 1e-8);
    };

    auto diffTest = [&](Step* step1, std::string name1, Step* step2,
                        std::string name2, double maxDiff = 0.0) {
      auto result1 = solve(step1, p.u, 0.0, 5);
      auto result2 = solve(step2, p.u, 0.0, 5);

      auto normDiff = p.energyNorm.diff(result1, result2);
      if (normDiff > maxDiff) {
        passed = false;
        std::cerr << name1 << " and " << name2 << " differ with norm "
                  << normDiff << "." << std::endl;
        return;
      }
      std::cout << Dune::formatString("%*s", -60, name1.c_str()) << " and "
                << Dune::formatString("%*s", -60, name2.c_str()) << " coincide"
                << ((maxDiff > 0.0)
                        ? " up to " + Dune::formatString("%e", maxDiff)
                        : ".") << std::endl;
    };

    {
      BlockGSStep<Matrix, Vector> gsStep;
      test(&gsStep, "BlockGS");
    }
    {
      BlockGSStep<Matrix, Vector> gsStep1;
      auto gsStep2 = Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(
          Dune::Solvers::BlockGS::LocalSolvers::direct(0.0));
      diffTest(&gsStep1, "BlockGS", &gsStep2, "Dune::Solvers::BlockGS(Direct)");
    }
    {
      TruncatedBlockGSStep<Matrix, Vector> gsStep1(1);
      auto gsStep2 = Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(
          Dune::Solvers::BlockGS::LocalSolvers::gs(0.0, 0.0));
      test(&gsStep1, "TruncatedBlockGS recursive");
      test(&gsStep2, "Dune::Solvers::BlockGS(GS)");
      diffTest(&gsStep1, "TruncatedBlockGS recursive", &gsStep2,
               "Dune::Solvers::BlockGS(GS)", 1e-8);
    }

    // test regularizable block GS
    for (auto reg : {false, true}) {
      for (auto type : {"Direct", "LDLt", "CG"}) {
        Dune::ParameterTree config;
        config["type"] = type;
        config["regularize_diag"] =
            reg ? "1e-8" : "0.0";     // for Dune::Solvers::*BlockGS
        config["cg_tol"] = "1e-14";   // for Dune::Solvers::BlockGS(CG)
        config["maxIter"] = "100";    // for Dune::Solvers::BlockGS
        config["tol"] = "1e-14";      // for Dune::Solvers::BlockGS

        auto gsStepPtr = Dune::Solvers::BlockGSStepFactory<
            Matrix, Vector>::createPtrFromConfig(config);
        auto gsString = Dune::formatString("Dune::Solvers::BlockGS(%s) %s",
                                           type, reg ? "regularized" : "");
        test(gsStepPtr.get(), gsString);
      }
    }

    // Test that ignoreNodes are honoured.
    {
      auto gsStep = Dune::Solvers::BlockGSStepFactory<Matrix, Vector>::create(
          Dune::Solvers::BlockGS::LocalSolvers::ldlt(0.0));

      Vector u_copy = p.u;
      for (size_t i = 0; i < p.u.size(); ++i)
        for (size_t j = 0; j < blocksize; ++j)
          if (p.ignore[i][j])
            u_copy[i][j] += 1.0;

      solve(&gsStep, u_copy, 1e-14, 2000);

      for (size_t i = 0; i < p.u.size(); ++i)
        for (size_t j = 0; j < blocksize; ++j)
          if (p.ignore[i][j])
            if (std::abs(u_copy[i][j] - 1.0 - p.u_ex[i][j]) > 1e-8) {
              std::cerr << "### error: ignoredNodes not respected!"
                        << std::endl;
              passed = false;
              break;
            }
    }

    // test projected block GS
    if (trivialDirichletOnly) // TODO: missing feature in ProjectedBlockGS
    {
      // Test 1: Without obstacles, we should get the same solution as with an
      // unconstrained solver
      size_t size = p.u.size();
      using GSStep = ProjectedBlockGSStep<Matrix, Vector>;
      typename GSStep::HasObstacle hasObstacle(size, false);
      {
        ProjectedBlockGSStep<Matrix, Vector> gsStep;
        gsStep.hasObstacle_ = &hasObstacle;
        test(&gsStep, "ProjectedBlockGS free");
      }
      hasObstacle.setAll();
      // Test2: Different initial iterates should lead to the same solution,
      // which should, furthermore, not intersect the obstacles
      {
        ProjectedBlockGSStep<Matrix, Vector> pGSStep1;
        pGSStep1.hasObstacle_ = &hasObstacle;
        pGSStep1.obstacles_ = &obstacles;
        ProjectedBlockGSStep<Matrix, Vector> pGSStep2;
        pGSStep2.hasObstacle_ = &hasObstacle;
        pGSStep2.obstacles_ = &obstacles;

        Vector u_copy1 = p.u;
        for (size_t i = 0; i < p.u.size(); ++i)
          for (size_t j = 0; j < blocksize; ++j)
            if (p.ignore[i][j])
              u_copy1[i][j] += 2.25;
        Vector u_copy2 = p.u;
        for (size_t i = 0; i < p.u.size(); ++i)
          for (size_t j = 0; j < blocksize; ++j)
            if (p.ignore[i][j])
              u_copy2[i][j] += 2.75;

        auto result1 = solve(&pGSStep1, u_copy1, 1e-14, 2000);
        auto result2 = solve(&pGSStep2, u_copy2, 1e-14, 2000);

        for (size_t i = 0; i < p.u.size(); ++i)
          for (size_t j = 0; j < blocksize; ++j)
            if (std::abs(result1[i][j] -
                         obstacles[i][j].projectIn(result1[i][j])) > 1e-14 or
                std::abs(result2[i][j] -
                         obstacles[i][j].projectIn(result2[i][j])) > 1e-14) {
              std::cerr << "### error: obstacles not respected!" << std::endl;
              passed = false;
              break;
            }
        auto normDiff = p.energyNorm.diff(result1, result2);
        // We cannot expect too much here (hence the low tolerance):
        // We are solving a potentially large system using nothing but
        // a smoother. Convergence will be very slow.
        if (normDiff > 1e-6) {
          std::cerr << "### error: Different initial iterates lead to "
                       "different solutions! (diff = "
                    << normDiff << ")" << std::endl;
          passed = false;
        }
      }
    }

    return passed;
  }
};

template<class TestSuite>
bool checkWithSmallGrids(TestSuite& suite)
{
    bool passed = true;

    passed = passed and checkWithGrid<Dune::YaspGrid<2, Dune::EquidistantOffsetCoordinates<double,2> > >(suite, 3);
    passed = passed and checkWithGrid<Dune::YaspGrid<3, Dune::EquidistantOffsetCoordinates<double,3> > >(suite, 1);

#if HAVE_UG
    passed = passed and checkWithGrid<Dune::UGGrid<2> >(suite, 2);
    passed = passed and checkWithGrid<Dune::UGGrid<3> >(suite, 1);
#endif

#if HAVE_DUNE_ALUGRID
    passed = passed and checkWithGrid<Dune::ALUGrid<2, 2, Dune::simplex, Dune::nonconforming>>(suite, 2);
    passed = passed and checkWithGrid<Dune::ALUGrid<3, 3, Dune::simplex, Dune::nonconforming>>(suite, 1);
#endif

    return passed;
}


int main(int argc, char** argv) {
  Dune::MPIHelper::instance(argc, argv);
  bool passed(true);

  GSTestSuite<1> testsuite1;
  passed &= checkWithSmallGrids(testsuite1);

  std::cout << std::endl;

  GSTestSuite<2> testsuite2;
  passed &= checkWithSmallGrids(testsuite2);

  std::cout << std::endl;

  GSTestSuite<2, false> testsuite2f;
  passed &= checkWithSmallGrids(testsuite2f);

  return passed ? 0 : 1;
}
